'use strict';

const Model = require('./model'),
      DiaryRelation = require('./diary_relation')

class ZModel extends DiaryRelation(Model) {
  static type() {
    return 'zqueries'
  }

  static get config() {
    return {
      permit: ['modelId', 'tokens'],
      findModels: {method: 'POST'} // Do a POST call to zqueries/all
    }
  }
}

module.exports = ZModel
