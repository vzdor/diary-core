'use strict';

const Model = require('./model'),
      guid = require('./guid')

class Diary extends Model {
  constructor() {
    super()
    // Set default attributes
    this.hideListing = false
  }

  static type() {
    return 'diaries'
  }

  static get config() {
    return {
      encrypt: {
        name: 'plain',
        hideListing: 'json'
      },
      permit: ['name', 'hideListing', 'localId']
    }
  }

  async deriveCrypto() {
    const text = this.localId + ':diary',
          crypto = this.constructor.crypto.secondaryCrypto({text})
    return crypto
  }

  attributes() {
    return {
      name: this.encrypted.name,
      hideListing: this.encrypted.hideListing,
      localId: this.localId
    }
  }

  async build({save} = {}) {
    // In case of a localId collision, generate a new localId on next save.
    if (this.isNew()) this.localId = guid()
    if (!save) return this.constructor.crypto.encryptModel(this)
  }
}

module.exports = Diary
