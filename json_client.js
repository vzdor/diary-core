'use strict';

const {StatusError, NetworkError, UnauthorizedError} = require('./errors'),
      Base64 = require('./base64')

class JsonClient {
  constructor({headers} = {}) {
    this.headers = headers || {'Content-type': 'application/json'}
  }

  async fetch(url, options) {
    return fetch(url, options)
  }

  auth({login, password}) {
    const s = Base64.encode(`${login}:${password}`)
    this.headers['Authorization'] = 'Basic ' + s
  }

  // options.status: valid status codes array
  // options.method: http method, POST, GET, ...
  // options.body: string
  async request(url, options = {}) {
    let response, status
    try {
      response = await this.fetch(url, {
        method: options.method || 'GET',
        body: options.body,
        headers: this._headers(options),
      })
    } catch(error) {
      console.error(error)
      throw new NetworkError(error.message)
    }
    // Shortcut
    status = response.status
    if ((options.status || [200]).includes(status)) return response.json()
    if (status == 401) throw new UnauthorizedError()
    throw new StatusError(status)
  }

  _headers(options = {}) {
    let headers = this.headers
    // https://fetch.spec.whatwg.org/#headers
    if (typeof Headers !== 'undefined') headers = new Headers(headers)
    return headers
  }
}

module.exports = JsonClient
