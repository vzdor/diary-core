'use strict';

function nodejsRandomBytes(size) {
  const crypto = require('crypto')
  return crypto.randomBytes(size)
}

function randomBytes(size) {
  let bytes
  if (typeof window === 'undefined') // Nodejs
    bytes = nodejsRandomBytes(size)
  else { // Use browser crypto
    bytes = new Uint8Array(size)
    crypto.getRandomValues(bytes)
  }
  return bytes
}

// Collisions:
// https://en.wikipedia.org/wiki/Universally_unique_identifier#Collisions
// https://en.wikipedia.org/wiki/Birthday_problem

// With 8 bytes, the probability of collision is 1/10^6
// after adding approx. 5.9 * 10^6 entries.

// Returns a random hex string.
// bytesSize: number of random bytes.
function guid(bytesSize = 8) {
  const bytes = randomBytes(bytesSize),
        strArray = Array.from(bytes).map((e) => {
          const s = e.toString(16)
          return '0'.repeat(2 - s.length) + s
        })
  return strArray.join('')
}

module.exports = guid
