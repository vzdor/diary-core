'use strict';

class Environment {
  constructor() {
    this.models = {}
  }

  model(Model) {
    const name = Model.name
    if (this.models[name] === undefined) this._setupModel(name, Model)
    return this.models[name]
  }

  newModel(Model, ... args) {
    const _Model = this.model(Model)
    return new _Model(... args)
  }

  clone() {
    const env = new this.constructor()
    Object.assign(env, this)
    env.models = []
    return env
  }

  _setupModel(name, Model) {
    const _Model = class extends Model {}
    this.models[name] = _Model
    _Model.env = this
  }
}

module.exports = Environment
