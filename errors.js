'use strict';

 class UnauthorizedError extends Error {
  constructor() {
    super('Unauthorized.')
    this.name = 'UnauthorizedError'
  }
}

class StatusError extends Error {
  constructor(status) {
    super(`Server returned unexpected status code: ${status}`)
    this.name = 'StatusError'
    this.status = status
  }
}

class NetworkError extends Error {
  constructor(error) {
    super('There was a problem communicating with the server.')
    this.error = error
    this.name = 'NetworkError'
  }
}

// returns error message. The message that we can show to the user
// if we did not handle the error.
function errorMessage(error) {
  if (['NetworkError', 'StatusError'].includes(error.name))
    return error.message
  return 'Application error.'
}

module.exports = {
  errorMessage,
  StatusError, NetworkError, UnauthorizedError
}
