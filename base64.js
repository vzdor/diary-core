'use strict';

function encode(text) {
  if (typeof btoa !== 'undefined') return btoa(text)
    // nodejs
  return (new Buffer(text)).toString('base64')
}

module.exports = {encode}
