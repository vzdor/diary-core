'use strict';

const Crypto = require('./model_crypto')

const Core = require('crypto-js/core'),
      Base64 = require('crypto-js/enc-base64'),
      HmacSHA256 = require('crypto-js/hmac-sha256'),
      AES = require('crypto-js/aes'),
      PBKDF2 = require('crypto-js/pbkdf2')

class JsCrypto extends Crypto {

  // For comments, see model_crypto.js.

  static pbkdf2(password, salt, iterations = 50000) {
    return new Promise((resolve) => {
      const key = PBKDF2(password, salt, {
        keySize: 256/32,
        iterations: iterations,
        hasher: Core.algo.SHA256
      })
      resolve(Base64.stringify(key))
    })
  }

  static initialize(options) {
    if (options.key) return this._fromKey(options.key)
    const {password, salt} = options
    return this.deriveKey(password, salt).then((key) => this._fromKey(key))
  }

  static _fromKey(key) {
    if (typeof key === 'string') key = Base64.parse(key)
    const crypto = new JsCrypto(this._deriveKeys(key))
    return Promise.resolve(crypto)
  }

  static _deriveKeys(key) {
    const encryptionKey = HmacSHA256('0', key),
          macKey = HmacSHA256('1', key),
          passwordKey = HmacSHA256('2', key),
          secondaryKey = HmacSHA256('3', key),
          servicePassword = HmacSHA256('0', passwordKey)
    return {
      passwordKey,
      macKey,
      encryptionKey,
      servicePassword: Base64.stringify(servicePassword),
      secondaryKey
    }
  }

  constructor({passwordKey, macKey, encryptionKey, servicePassword, secondaryKey}) {
    super()
    if (typeof passwordKey === 'string') {
      passwordKey = Base64.parse(passwordKey)
      macKey = Base64.parse(macKey)
      encryptionKey = Base64.parse(encryptionKey)
      secondaryKey = Base64.parse(secondaryKey)
    }
    this._passwordKey = passwordKey
    this._macKey = macKey
    this._encryptionKey = encryptionKey
    this._secondaryKey = secondaryKey
    this.servicePassword = servicePassword
  }

  get passwordKey() { return Base64.stringify(this._passwordKey) }

  get macKey() { return Base64.stringify(this._macKey) }

  get encryptionKey() { return Base64.stringify(this._encryptionKey) }

  get secondaryKey() { return Base64.stringify(this._secondaryKey) }

  encrypt(text, {iv} = {}) {
    if (iv)
      iv = Base64.parse(iv)
    else
      // https://github.com/brix/crypto-js/issues/7#issuecomment-41379211
      // From a github post, cryptojs random generator is
      // https://en.wikipedia.org/wiki/Linear_congruential_generator
      iv = Core.lib.WordArray.random(16)

    const encrypted = Core.AES.encrypt(text, this._encryptionKey, {
      padding: Core.pad.Pkcs7, // Make explicit for clarity.
      mode: Core.mode.CBC,
      iv: iv
    })

    return JSON.stringify({
      encrypted: Base64.stringify(encrypted.ciphertext),
      iv: Base64.stringify(iv)
    })
  }

  decrypt(text) {
    const encryptedObj = JSON.parse(text),
          // decrypt cfg.format is OpenSSL which is using Base64, so do not parse.
          ciphertext = encryptedObj.encrypted,
          iv = Base64.parse(encryptedObj.iv)

    const decrypted = Core.AES.decrypt(ciphertext, this._encryptionKey, {
      padding: Core.pad.Pkcs7,
      mode: Core.mode.CBC,
      iv: iv
    })

    return decrypted.toString(Core.enc.Utf8)
  }

  hmac(text) {
    const encrypted = HmacSHA256(text, this._macKey)
    return Base64.stringify(encrypted)
  }

  async secondaryCrypto({text}) {
    const key = HmacSHA256(text, this._secondaryKey),
          crypto = this.constructor.initialize({key})
    return crypto
  }
}

module.exports = JsCrypto
