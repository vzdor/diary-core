'use strict';

const Core = require('crypto-js/core'),
      Base64 = require('crypto-js/enc-base64'),
      HmacSHA256 = require('crypto-js/hmac-sha256')

class ZCrypto {
  // It is async for WebCrypto API compatibility.

  // key: base64 string.
  static async initialize({key}) {
    key = Base64.parse(key)
    return ZCrypto._deriveKeys(key)
  }

  static async _deriveKeys(key) {
    let derivedKeys = new Array(1)
    for (let i = 0; i < derivedKeys.length; i++) {
      derivedKeys[i] = HmacSHA256(i.toString(), key)
    }
    return new ZCrypto({keys: derivedKeys})
  }

  constructor({keys}) {
    this.keys = keys
  }

  // Resolves with a trapdoor for word w:
  // [x1, x2, ..., x_n]. n = keys.length.
  // x1, x2, ... HMAC SHA256 in Base64 format.
  async trapdoor(w) {
    return this.keys.map((key) => HmacSHA256(w, key).toString(Base64))
  }

  // Resolves with codeword, an array with internal structures.
  async codeword(trapdoor, id) {
    return trapdoor.map((x) => HmacSHA256(id, Base64.parse(x)))
  }
}

module.exports = ZCrypto
