'use strict';

const Store = require('./store'),
      Urls = require('./urls'),
      Client = require('./json_client')

class HttpStore extends Store {
  // root: the root url.
  constructor({root = '', client} = {}) {
    super()
    this.root = root
    this.cl = client || new Client()
  }

  signIn(options) {
    this.cl.auth(options)
  }

  // url(Entry, {limit: 100})
  // returns: /diaries/1/entries/?limit=100
  url(Model, options = {}) {
    const optionsCopy = Object.assign({}, options),
          url = this._url(Model, optionsCopy.id, [this.root]).join('/')
    delete optionsCopy.id
    return url + Urls.urlQuery(optionsCopy)
  }

  // Returns a promise.
  destroyModel(model) {
    const url = this.url(model.constructor, {id: model.id}),
          options = {
            method: 'DELETE',
            status: [200]
          }
    return this.cl.request(url, options)
  }

  _url(Model, id, ary = []) {
    const directory = Model.directoryModel
    if (directory) this._url(directory.constructor, directory.id, ary)
    ary.push(Model.type())
    if (id) ary.push(id.toString())
    return ary
  }

  _find(Model, id) {
    return this.cl.request(this.url(Model, {id: id}), {
      method: 'GET',
      status: [200, 404]
    })
  }

  _findAll(Model, options = {}) {
    const defaults = {findModels: {method: 'GET'}},
          config = Object.assign(defaults, Model.config || {})
    if (config.findModels.method === 'POST')
      return this._findAllPOST(Model, options)
    const _options = Object.assign({}, options, {
      method: 'GET',
      status: [200]
    })
    return this.cl.request(this.url(Model, options.query), _options)
  }

  // Workaround webserver max query length.
  _findAllPOST(Model, options = {}) {
    const postfix = '/all',
          _options = Object.assign({}, options, {
            method: 'POST',
            body: JSON.stringify(options.query),
            status: [200]
          })
    return this.cl.request(this.url(Model) + postfix, _options)
  }

  _save(model) {
    const url = this.url(model.constructor, {id: model.id}),
          opts = {
            body: JSON.stringify(model),
            method: model.isNew() ? 'POST' : 'PUT',
            status: [201, 400]
          }
    return this.cl.request(url, opts)
  }
}

module.exports = HttpStore
