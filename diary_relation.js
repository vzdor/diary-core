'use strict';

const DiaryRelation = function(Model) {
  return class extends Model {
    static get directoryModel() {
      return this.env.diary
    }

    static get crypto() {
      return this.env.diaryCrypto
    }
  }
}

module.exports = DiaryRelation
