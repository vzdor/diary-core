'use strict';

class JsonapiSerializer {
  serialize(model) {
    return {
      data: this._serializeData(model)
    }
  }

  _serializeData(model) {
    let data = {},
        attributes = model.attributes()
    data.type = model.constructor.type()
    if (model.id) data.id = model.id
    if (attributes) data.attributes = attributes
    return data
  }

  fromSerialized(obj, options = {}) {
    if (obj.errors) return obj
    if (options.includeMeta)
      return {
        meta: obj.meta,
        attributes: this._fromData(obj.data)
      }
    return this._fromData(obj.data)
  }

  _fromData(data) {
    if (data instanceof Array)
      return data.map(this._fromData)
    let obj = Object.assign({}, data.attributes)
    if (data.id) obj.id = data.id
    return obj
  }
}

class DummySerializer {
  serialize(model) {
    let obj = {}
    let attrs = model.attributes()
    if (attrs) Object.assign(obj, attrs)
    if (model.id) obj.id = model.id
    return obj
  }

  fromSerialized(obj, options = {}) {
    return obj
  }
}

module.exports = {
  Jsonapi: JsonapiSerializer,
  Dummy: DummySerializer
}
