'use strict';

const Entry = require('./entry'),
      Indexer = require('./zindexer'),
      TfIdf = require('stemtiny/tf_idf')

function defaults(options = {}) {
  options.perPage = options.perPage || 10
  return options
}

function PagedFinder(Model, options = {}) {
  const {perPage} = defaults(options)

  this.find = async function(page) {
    const result = await Model.findModels({
      includeMeta: true,
      query: {page, perPage}
    })
    this.pagesCount = result.meta.totalCount / perPage
    return result.models
  }
}

function PagedTextFinder(options = {}) {
  const {perPage, text, env} = defaults(options)

  const Model = env.model(Entry)

  let docIds, // The ids of documents matching the query, it is an object.
      ordered, // Sorted docIds array.
      indexer = new Indexer({env}),
      finder = indexer.find(text) // A promise.

  // Set docIds and ordered.
  finder.then((_docIds) => {
    docIds = _docIds // [{id: entry.localId, tokens: [...]}, ...]
    ordered = order() // Rank and sort.
    this.pagesCount = ordered.length / perPage
  })

  // Initialization end.

  function order() {
    let tfIdf = new TfIdf(docIds)
    return tfIdf.order()
  }

  function select(page) {
    const startAt = page * perPage, endAt = (page + 1) * perPage
    return ordered.slice(startAt, endAt)
  }

  // selected: scores array.
  // Make an object from selected and docIds:
  //  {localId: {score: 1.1, tokens: []}, ...}
  function join(selected) {
    return selected.reduce((obj, score) => {
      // id is a localId.
      obj[score.id] = {score: score.score, tokens: docIds[score.id]}
      return obj
    }, {})
  }

  async function findModels(selected) {
    return Model.findModels({
      query: {
        localId: selected.map((obj) => obj.id)
      }
    })
  }

  // Interface

  this.find = async function(page) {
    await finder
    if (ordered.length == 0) return [] // Not found
    const selected = select(page), // Select for this page from ordered.
          joined = join(selected),
          models = await findModels(selected)
    // Copy joined attributes to the model, and sort the models
    // by score.
    models.forEach((model) =>  {
      Object.assign(model, joined[model.localId])
    })
    // Higher score -- better match.
    return models.sort((m1, m2) => m2.score - m1.score)
  }
}

module.exports = {PagedFinder, PagedTextFinder}
