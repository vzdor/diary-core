'use strict';

require('./test_helper')

const urls = require('../urls'),
      assert = require('assert')

describe('urls', function() {
  describe('urlQuery', function() {
    it('returns query', function() {
      const queries = ['?key1=1&key2=2', '?key2=2&key1=1'], // We do not know in what order
            q = urls.urlQuery({key1: 1, key2: 2})
      assert(queries.includes(q))
    })

    it('filters undefined keys', function() {
      assert.equal(urls.urlQuery({key1: 1, key2: undefined}), '?key1=1')
    })
  })
})
