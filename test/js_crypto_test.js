require('./test_helper')

const Crypto = require('../js_crypto'),
      assert = require('assert')

const testKey = '505nT7v2taqhjyWdU5uNaqGi3+n+88R829WyfjB+2qg='
// Derived from testKey:
const cryptoOptions = {
  encryptionKey: '78ti740GdMom+Ws95gQMmWd8HyTK+q+U4TLjwNRcIQw=',
  macKey: 'ea4PJFoYn/8Brmi1L6/NXvN8FEzPJTwXqMTuU2grzG4=',
  passwordKey: 'x9gFAkrhbOF7XsJyJowMc/Yov7zO7YmzkfE6co3DnlU=',
  servicePassword: 'W57z0kKVE2pTB+n2MFISqepTgIa5RuQSr/GRkvZ9MYE=',
  secondaryKey: 'hFhkruooTPomcimm/W1oL2W1HORXDfRHd772ON80PGI='
}

function newCrypto() {
  return new Crypto(cryptoOptions)
}

describe('Crypto', function() {
  describe('initialize', function() {
    it('resolves with crypto', async function() {
      const crypto = await Crypto.initialize({key: testKey})
      assert(crypto instanceof Crypto)
    })

    context('with {password, salt}', function() {
      it('calls deriveKey', async function() {
        class TestCrypto extends Crypto {
          static deriveKey(password, salt) {
            return Promise.resolve(testKey)
          }
        }
        const crypto = await TestCrypto.initialize({password: 'p', salt: 's'})
        assert.equal(crypto.macKey, 'ea4PJFoYn/8Brmi1L6/NXvN8FEzPJTwXqMTuU2grzG4=')
      })
    })

    it('computes macKey', function() {
      return Crypto.initialize({key: testKey}).then((crypto) => {
        assert.equal(crypto.macKey, 'ea4PJFoYn/8Brmi1L6/NXvN8FEzPJTwXqMTuU2grzG4=')
      })
    })

    it('computes passwordKey', function() {
      return Crypto.initialize({key: testKey}).then((crypto) => {
        assert.equal(crypto.passwordKey, 'x9gFAkrhbOF7XsJyJowMc/Yov7zO7YmzkfE6co3DnlU=')
      })
    })

    it('computes encryptionKey', function() {
      return Crypto.initialize({key: testKey}).then((crypto) => {
        assert.equal(crypto.encryptionKey, '78ti740GdMom+Ws95gQMmWd8HyTK+q+U4TLjwNRcIQw=')
      })
    })

    it('computes servicePassword', function() {
      return Crypto.initialize({key: testKey}).then((crypto) => {
        assert.equal(crypto.servicePassword, 'W57z0kKVE2pTB+n2MFISqepTgIa5RuQSr/GRkvZ9MYE=')
      })
    })

    it('computes secondaryKey', function() {
      return Crypto.initialize({key: testKey}).then((crypto) => {
        assert.equal(crypto.secondaryKey, 'hFhkruooTPomcimm/W1oL2W1HORXDfRHd772ON80PGI=')
      })
    })
  })

  describe('pbkdf2', function() {
    // sjcl.
    it('resolves with a pbkdf2-sha256, using salt, base64 string', function() {
      return Crypto.pbkdf2('hello', 'salt', 10).then((key) => {
        assert.equal(key, '2eGsyj0fTIPGo7z/HnIdgsMThsjiUVWbPTfKjCSa8AU=')
      })
    })

    it('computes iterations', function() {
      return Crypto.pbkdf2('hello', 'salt', 2).then((key) => {
        assert.equal(key, 'frgG6foE4NTAat66WlDGr2hWAxMjaJUZKDWebBAxZjs=')
      })
    })
  })

  describe('#encrypt', function() {
    it('returns serialized {encrypted, iv} object', function() {
      const text = 'hello',
            encrypted = newCrypto().encrypt(text),
            obj = JSON.parse(encrypted)
      assert(obj.encrypted)
      assert(obj.iv)
    })

    // aes result in the tests is from aes-js, a different library.

    context('less bytes than in the block', function() {
      it('returns Pkcs7, CBC encrypted with encryptionKey', function() {
        const text = 'hello',
              encrypted = newCrypto().encrypt(text, {
                iv: 'dfDeTvriHbLp5BUQzTF3TQ=='
              }),
              obj = JSON.parse(encrypted)
        assert.equal(obj.encrypted, 'nUJcpyFcuzAZ9t01akm8HQ==')
      })
    })

    it('returns Pkcs7, CBC encrypted with encryptionKey', function() {
      const text = '1234567890 1234567890',
            encrypted = newCrypto().encrypt(text, {
              iv: 'dfDeTvriHbLp5BUQzTF3TQ=='
            }),
            obj = JSON.parse(encrypted)
      assert.equal(obj.encrypted, 'ZgzDSIfX5WIlxJ2oPVEO4l1qjAo4abNigNmrpu5QG5w=')
    })
  })

  describe('#decrypt', function() {
    it('decrypts its own encrypted', function() {
      const text = 'hello',
            encrypted = newCrypto().encrypt(text),
            decrypted = newCrypto().decrypt(encrypted)
      assert.equal(decrypted, text)
    })
  })

  // sjcl.
  describe('#hmac', function() {
    it('retruns hmac-sha256 using macKey', function() {
      const hash = newCrypto().hmac('hello')
      assert.equal(hash, '2gCCq24uNpB8AfDKwSorxaj+thnTDVtOtVa2HSL8fUw=')
    })
  })

  describe('#secondaryCrypto', function() {
    it('resolves with new crypto using secondaryKey', async function() {
      const mainCrypto = newCrypto(),
            crypto = await mainCrypto.secondaryCrypto({text: '1'})
      assert.equal(crypto.encryptionKey, 'E6N0i52SJHGJLIkAnBwFnTltM2ODcLe2LcxyXMokjOQ=')
    })
  })
})
