'use strict';

require('./test_helper')

const Diary = require('../diary'),
      Cryptos = require('../test_cryptos')

const assert = require('assert')

describe('Diary', function() {
  describe('#attributes', function() {
    it('returns attrs', async function() {
      const diary = Diary.fromAttributes({localId: '1'})
      diary.encrypted = {name: 'hello', hideListing: 'true'}
      assert.deepEqual(diary.attributes(), {
        name: 'hello',
        localId: '1',
        hideListing: 'true'
      })
    })
  })

  describe('#save', function() {
    after(function() {
      Diary.env.store.clear()
    })

    it('generates localId string', async function() {
      const diary = new Diary()
      await diary.save()
      assert(typeof diary.localId === 'string')
      assert.equal(diary.localId.length, 16)
    })

    it('generates new localId if isNew', async function() {
      const diary = Diary.fromAttributes({localId: '1'})
      await diary.save()
      assert.notEqual(diary.localId, '1')
    })

    it('keeps localId', async function() {
      const diary = Diary.fromAttributes({id: 1, localId: '1'})
      await diary.save()
      assert.equal(diary.localId, '1')
    })

    it('encrypts attributes', async function() {
      const env = Diary.env.clone(),
            diary = env.model(Diary).fromAttributes({
              name: 'hello',
              hideListing: true
            })
      env.crypto = new Cryptos.SimpleCrypto()
      await diary.save()
      const attrs = env.store.pages.diaries[diary.id]
      assert.equal(attrs.name, 'crypted,hello')
      assert.equal(attrs.hideListing, 'crypted,true')
    })
  })

  describe('#deriveCrypto', function() {
    it('uses localId:diary as secondaryCrypto argument', async function() {
      const env = Diary.env.clone()
      env.crypto = new Cryptos.SimpleCrypto()
      env.crypto.secondaryCrypto = async function({text}) { return text }
      const diary = env.model(Diary).fromAttributes({localId: 1}),
            crypto = await diary.deriveCrypto()
      assert.equal(crypto, '1:diary')
    })
  })
})
