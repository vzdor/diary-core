require('./test_helper')

let assert = require('assert')

let Model = require('../model'),
    Store = require('../store'),
    Cryptos = require('../test_cryptos')

class TestModel extends Model {
  static type() {
    return 'entries'
  }

  static get config() {
    return {
      encrypt: ['message'],
      permit: ['message']
    }
  }

  static get crypto() { return new Cryptos.SimpleCrypto() }

  attributes() {
    return {
      message: this.message
    }
  }
}

describe('Store', function() {
  describe('#findModels', function() {
    class TestStore extends Store {
      async _findAll(Model, options = {}) {
        return [{id: 1, message: 'crypted,hello'}]
      }
    }

    it('resolves with decrypted models', async function() {
      const s = new TestStore(),
            models = await s.findModels(TestModel)
      assert.equal(models[0].message, 'hello')
    })

    context('with includeMeta', function() {
      it('resolves with meta and decrypted models', async function() {
        class TestStore extends Store {
          async _findAll(Model, options = {}) {
            return {
              meta: {count: 100},
              attributes: [{id: 1, message: 'crypted,hello'}]
            }
          }
        }
        const s = new TestStore(),
              obj = await s.findModels(TestModel, {includeMeta: true})
        assert.deepEqual(obj.meta, {count: 100})
        assert(obj.models)
        const model = obj.models[0]
        assert(model instanceof TestModel)
        assert.equal(model.message, 'hello')
      })
    })
  })

  describe('#findModel', function() {
    class TestStore extends Store {
      async _find(Model, options = {}) {
        return {id: 1, message: 'crypted,hello', x: 'x'}
      }
    }

    it('resolves with decrypted model', async function() {
      const s = new TestStore(),
            model = await s.findModel(TestModel, 1)
      assert.equal(model.message, 'hello')
    })

    it('filters not permitted attributes', async function() {
      const s = new TestStore(),
            model = await s.findModel(TestModel, 1)
      assert.equal(model.x, undefined)
      assert.equal(model.id, 1)
    })
  })

  describe('#saveModel', function() {
    class TestStore extends Store {
      async _save(model) {
        return {id: 1, message: 'crypted,hello', x: 'x'}
      }
    }

    it('decrypts returned attributes', async function() {
      const s = new TestStore(),
            model = new TestModel()
      await s.saveModel(model)
      assert(model.id)
      assert(model.message, 'hello')
    })

    it('filters not permitted attributes', async function() {
      const s = new TestStore(),
            model = new TestModel()
      await s.saveModel(model)
      assert.equal(model.x, undefined)
      assert.equal(model.id, 1)
    })

    it('permits id and errors attributes', async function() {
      const s = new TestStore(),
            model = new TestModel()
      s._save = async function() {
        return {id: 1, errors: {x: 1}}
      }
      await s.saveModel(model)
      assert.equal(model.id, 1)
      assert.deepEqual(model.errors, {x: 1})
    })

    it('calls model.build', function(done) {
      const s = new TestStore(),
            model = new TestModel()
      model.build = async function() {
        done()
      }
      s.saveModel(model)
    })
  })
})
