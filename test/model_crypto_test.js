'use strict';

require('./test_helper')

const {SimpleCrypto} = require('../test_cryptos'),
      Model = require('../model')

const assert = require('assert')

const crypto = new SimpleCrypto()

class TestModel extends Model {
  static get config() {
    return {
      encrypt: ['message']
    }
  }

  static get crypto() {
    return crypto
  }
}

describe('Crypto', function() {
  describe('passwordKey', function() {
    it('calls pbkdf2 with iterations equal to 50000', function() {
      class TestCrypto extends SimpleCrypto {
        static pbkdf2(password, salt, iterations) {
          return iterations
        }
      }
      assert.equal(TestCrypto.deriveKey('hello', 'bob@diary'), 50000)
    })
  })

  context('with config.encrypt object', function() {
    class NewModel extends TestModel {
      static get config() {
        return {
          encrypt: {
            name: 'plain',
            isVisible: 'json'
          }
        }
      }
    }

    describe('#encryptModel', function() {
      it('sets encrypted attribute', async function() {
        const model = NewModel.fromAttributes({
          name: 'Bob',
          isVisible: false
        })
        await crypto.encryptModel(model)
        assert.deepEqual(model.encrypted, {
          name: 'crypted,Bob',
          isVisible: 'crypted,false'
        })
      })
    })

    describe('#descyptModel', function() {
      it('resolves with decrypted attributes', async function() {
        const encrypted = {
          name: 'crypted,Bob',
          isVisible: 'crypted,true'
        },
              attrs = await crypto.decryptModel(encrypted, NewModel)
        assert.deepEqual(attrs, {name: 'Bob', isVisible: true})
      })

      it('ignores missing attributes', async function() {
        const encrypted = {
          name: 'crypted,Bob',
        }
        let error
        try {
          await crypto.decryptModel(encrypted, NewModel)
        } catch(er) {
          error = er
        }
        assert(!error, error)
      })
    })
  })

  describe('#encryptModel', async function() {
    it('sets encrypted attribute', async function() {
      let model = new TestModel()
      model.message = 'hello'
      await crypto.encryptModel(model)
      assert.deepEqual(model.encrypted, {message: 'crypted,hello'})
    })
  })

  describe('#decryptModel', function() {
    it('resolves with decrypted attributes', async function() {
      const encrypted = {message: 'crypted,hello'},
            attrs = await crypto.decryptModel(encrypted, TestModel)
      assert.deepEqual(attrs, {message: 'hello'})
    })
  })
})
