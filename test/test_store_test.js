'use strict';

require('./test_helper')

const TestStore = require('../test_store'),
      Model = require('../model'),
      Cryptos = require('../test_cryptos'),
      Registration = require('../registration')

const assert = require('assert')

class TestModel extends Model {
  static type() { return 'tests' }

  static get config() {
    return {
      permit: [
        'message'
      ]
    }
  }

  attributes() {
    return {
      message: this.message
    }
  }
}

describe('TestStore', function() {
  function newModel() {
    return TestModel.fromAttributes({message: 'hello'})
  }

  describe('#_findAll', function() {
    it('returns a copy', async function() {
      const s = new TestStore()
      await s.saveModel(newModel())
      const attrs = await s._findAll(TestModel)
      assert.equal(attrs.length, 1)
      attrs[0].message = 'test'
      assert.equal(s.pages.tests[1].message, 'hello')
    })
  })

  describe('#_find', function() {
    it('returns a copy', async function() {
      const s = new TestStore(),
            model = newModel()
      await s.saveModel(model)
      const attrs = await s._find(TestModel, model.id)
      attrs.message = 'test'
      assert.equal(s.pages.tests[1].message, 'hello')
    })
  })

  describe('#_save', function() {
    it('returns a copy', async function() {
      const s = new TestStore()
      const attrs = await s._save(newModel())
      attrs.message = 'test'
      assert.equal(s.pages.tests[1].message, 'hello')
    })

    context('with persisted model', function() {
      it('returns a copy', async function() {
        const s = new TestStore(),
              model = newModel()
        let attrs = await s._save(model)
        model.id = attrs.id
        attrs = await s._save(model)
        attrs.message = 'test'
        assert.equal(s.pages.tests[1].message, 'hello')
      })
    })
  })

  describe('#save', function() {
    context('with registration', function() {
      it('adds diaries and assigns diary id', async function() {
        const model = new Registration(),
              s = new TestStore()
        model.email = 'bob@diary'
        await s.saveModel(model)
        assert.equal(s.pages.diaries[1].name, 'Default')
        assert.equal(model.diaries[0].id, 1)
        assert.equal(model.diaries[1].id, 2)
      })
    })
  })
})
