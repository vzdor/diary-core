'use strict';

const Entry = require('../cuckoo_entry')

const assert = require('assert')

describe('CuckooEntry', function() {
  it('builds ints', function() {
    const words = {
      words: [(1 << 16) | 2, 3 << 16, 4]
    },
          e = new Entry(words)
    assert.equal(e.int1, 1)
    assert.equal(e.int2, 2)
    assert.equal(e.int3, 3)
    assert.equal(e.fp, 4)
  })

  describe('max', function() {
    it('returns max value that an int* can take', function() {
      assert.equal(Entry.max, 0xffff - 1)
    })
  })
})
