'use strict';

require('./test_helper')

const Zindexer = require('../zindexer'),
      Zmodel = require('../zmodel'),
      Cuckoo = require('../cuckoo')

const assert = require('assert')

describe('Zindexer', function() {
  describe('#addDoc', function() {
    it('resolves with index', async function() {
      let indexer = new Zindexer({env: Zmodel.env}),
          index = await indexer.addDoc({id: 1, text: 'hello'})
      assert(index)
    })

    it('resolves with cuckoo', async function() {
      const indexer = new Zindexer({env: Zmodel.env}),
            index = await indexer.addDoc({id: 1, text: 'hello world'})
      assert(index instanceof Cuckoo)
      const ary = index.fingerprints()
      assert.equal(ary[9], 34000)
      assert.equal(ary[11], 31507)
    })

    it('uses secondary key of diary crypto', async function() {
      async function fingerprints(env) {
        const indexer = new Zindexer({env: env}),
              index = await indexer.addDoc({id: 1, text: 'hello world'})
        return index.fingerprints()
      }

      const env = Zmodel.env.clone()
      env.diaryCrypto = {secondaryKey: '78ti740GdMom+Ws95gQMmWd8HyTK+q+U4TLjwNRcIQw='}
      // Make sure we use different secondary keys
      assert.notEqual(env.diaryCrypto.secondaryKey, Zmodel.env.diaryCrypto.secondaryKey)

      const ary1 = await fingerprints(env),
            ary2 = await fingerprints(Zmodel.env)
      assert.notDeepEqual(ary1, ary2, 'same arrays')
    })
  })

  describe('#find', function() {
    it('resolves with docIds in find format', async function() {
      const m1 = new Zmodel(),
            m2 = new Zmodel(),
            indexer = new Zindexer({env: Zmodel.env})
      await m1.save(indexer)
      await m2.save(indexer)
      Object.assign(Zmodel.store.pages.zqueries[m1.id], {
        tokens: [true, false],
      })
      Object.assign(Zmodel.store.pages.zqueries[m2.id], {
        tokens: [true, true],
      })
      const results = await indexer.find('hello world')
      assert(results)
      assert.deepEqual(results[m1.id], ['hello'])
      assert.deepEqual(results[m2.id], ['hello', 'world'])
    })
  })
})
