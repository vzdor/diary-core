'use strict';

require('./test_helper')

let Model = require('../model')
let Cryptos = require('../test_cryptos')
let assert = require('assert')

class TestModel extends Model {
  static type() {
    return 'entries'
  }

  attributes() {
    return {
      message: this.message
    }
  }
}

describe('Model', function() {
  after(function() {
    TestModel.store.clear()
  })

  describe('fromAttributes', function() {
    it('returns models array', function() {
      let obj = [
        {
          id: 1,
          message: 'hello'
        },
        {
          id: 2,
          message: 'world',
        }
      ]
      let models = TestModel.fromAttributes(obj)
      assert.equal(models[0].id, 1)
      assert.equal(models[1].message, 'world')
    })
  })

  context('with new model', function() {
    function newModel() {
      return new TestModel()
    }

    describe('#attributes', function() {
      it('returns empty attributes', function() {
        assert.deepEqual(newModel().attributes(), {message: null})
      })
    })

    describe('#isNew', function() {
      it('returns true', function() {
        assert(newModel().isNew())
      })
    })

    describe('#errors', function() {
      it('returns an empty object', function() {
        assert.deepEqual(newModel().errors, {})
      })
    })

    describe('#encrypted', function() {
      it('returns an empty object', function() {
        assert.deepEqual(newModel().encrypted, {})
      })
    })
  })

  context('when persisted', function() {
    describe('#isNew', function() {
      it('returns false', function() {
        let model = new TestModel()
        model.id = 1
        assert.equal(model.isNew(), false)
      })
    })
  })

  describe('#save', function() {
    it('clears model errors', async function() {
      let model = new TestModel()
      model.errors = {message: ['blah']}
      await model.save()
      assert.deepEqual(model.errors, {})
    })

    context('with null attributes', function() {
      it('resolves', async function() {
        let model = new Model()
        model.attributes = () => null
        await model.save()
      })
    })

    it('resolves with model', async function() {
      const model = new TestModel(),
            result = await model.save()
      assert(result === model)
    })

    it('calls build', async function() {
      const model = new TestModel()
      model.build = function({save} = {}) {
        assert(save)
        this.x = 1
      }
      await model.save()
      assert.equal(model.x, 1)
    })
  })

  describe('#clone', function() {
    it('returns a copy', function() {
      let model = new TestModel()
      model.message = 'hello'
      let newModel = model.clone()
      newModel.message = 'new message'
      assert.equal(model.message, 'hello')
    })
  })

  describe('find', function() {
    before(function() {
      let attrs = {
        id: 1,
        message: 'hello',
      }
      TestModel.store.pages['entries'] = {
        1: attrs
      }
    })

    it('resolves with model', async function() {
      const model = await TestModel.find(1)
      assert.equal(model.id, 1)
      assert.equal(model.message, 'hello')
      assert(model instanceof TestModel)
    })
  })

  describe('destroy', function() {
    it('calls store destroyModel', function(done) {
      const env = TestModel.env.clone()
      env.store = new env.store.constructor()
      env.store.destroyModel = function() {
        done()
      }
      env.newModel(TestModel).destroy()
    })
  })
})
