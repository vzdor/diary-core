'use strict';

const Cuckoo = require('../cuckoo')

const assert = require('assert'),
      crypto = require('crypto')

class Entry {
  constructor(ary) {
    this.ary = ary
    this.fp = (ary[ary.length - 2] << 8) + (ary[ary.length - 1])
    this.int1 = (ary[0] << 8) + ary[1]
    this.int2 = (ary[2] << 8) + ary[3]
    this.int3 = (ary[4] << 8) + ary[5]
  }
}

Entry.max = 0xffff - 1 // Maximum number an entry.int* can take.

class TestCuckoo extends Cuckoo {
  constructor(options = {}) {
    options.Entry = Entry
    super(options)
  }

  exists(s) { // I do not need this for the app.
    let e = new this.Entry(s),
        i1 = e.int1 % this.size,
        i2 = e.int2 % this.size,
        i3 = e.int3 % this.size
    if ((this.ary[i1] !== undefined && this.ary[i1].fp === e.fp) ||
        (this.ary[i2] !== undefined && this.ary[i2].fp === e.fp) ||
        (this.ary[i3] !== undefined && this.ary[i3].fp === e.fp))
      return true
    return false
  }
}

const randomBytes = crypto.randomBytes

describe('Cuckoo', function() {
  it('packages all entries with load factor', function() {
    const ary0 = [],
          co = new TestCuckoo({size: 48})
    for (let i = 0; i < 120; i++) {
      let s = randomBytes(32)
      co.add(s)
      ary0.push(s)
    }
    // All entries in the dictionary?
    for (let s of ary0) assert(co.exists(s))
    assert(co.loadFactor() > 0.7)
  })

  // it('keeps false positives small', function() {
  //   let co = new TestCuckoo({size: 1000}), fps = 0, n = 10000
  //   for (let i = 0; i < 500; i++) {
  //     let s = randomBytes(32)
  //     co.add(s)
  //   }
  //   // this.timeout(100000) // change test timeout.
  //   for (let i = 0; i < n; i++) {
  //     let s = randomBytes(32)
  //     if (co.exists(s)) fps += 1
  //   }
  //   assert(fps/n < 1/1000)
  // })

  // it('keeps collisions small', function() {
  //   let co = new TestCuckoo({size: 65000}),
  //       n = 50000,
  //       c = 0
  //   for (let i = 0; i < n; i++) {
  //     let s = randomBytes(32)
  //     if (co.exists(s)) c += 1
  //     co.add(s)
  //   }
  //   assert(c/n < 1/1000)
  // })

  describe('#toJSON', function() {
    it('returns fingerprints string', function() {
      const co = new TestCuckoo({size: 4}),
            bytes = new Array(32)
      bytes.fill(1)
      co.add(bytes)
      const s = co.toJSON()
      assert.equal(s, ',257,,')
    })
  })
})
