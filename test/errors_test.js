require('./test_helper')

const Errors = require('../errors'),
      assert = require('assert')

describe('Errors', function() {
  describe('errorMessage', function() {
    it('returns a text message for an uknown error', function() {
      assert.equal(Errors.errorMessage({}), 'Application error.')
    })

    it('returns error message', function() {
      const err = new Errors.StatusError(100)
      assert.equal(Errors.errorMessage(err), 'Server returned unexpected status code: 100')
    })
  })
})
