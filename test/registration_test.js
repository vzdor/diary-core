'use strict';

require('./test_helper')

const Registration = require('../registration'),
      {SimpleCrypto} = require('../test_cryptos')

const assert = require('assert')

const env = Registration.env.clone(),
      _Registration = env.model(Registration)
env.crypto = new SimpleCrypto()

describe('Registration', function() {
  beforeEach(function() {
    _Registration.store.clear()
  })

  describe('#save', function() {
    it('saves user and encrypted diaries', async function() {
      const registration = _Registration.fromAttributes({
        email: 'bob@diary'
      })
      await registration.save()
      const attrs = env.store.pages.registrations[1],
            [d0, d1] = attrs.diaries
      assert.deepEqual(attrs.user, {
        email: 'bob@diary',
        password: env.crypto.servicePassword
      })
      assert.equal(d0.name, 'crypted,Default')
      assert.equal(d0.hideListing, 'crypted,false')
      assert(d0.localId)
      assert.equal(d1.name, 'crypted,Secrets')
      assert.equal(d1.hideListing, 'crypted,true')
      assert(d1.localId)
    })
  })

  describe('#build', function() {
    it('initialized diaries')
  })

  describe('#setAttributes', function() {
    it('assigns diary ids but keeps name', async function() {
      const registration = new _Registration()
      await registration.save()
      assert.equal(registration.diaries[0].id, 1)
      assert.equal(registration.diaries[0].name, 'Default')
      assert.equal(registration.diaries[1].id, 2)
    })
  })
})
