require('./test_helper')

let assert = require('assert')

let Model = require('../model'),
    HttpStore = require('../http_store'),
    Client = require('../json_client'),
    Cryptos = require('../test_cryptos')

class TestClient extends Client {
  constructor({resolveWith = {}}) {
    super()
    this.calls = []
    this.resolveWith = resolveWith
  }

  request(url, options = {}) {
    this.calls.push({url, options})
    return Promise.resolve(this.resolveWith)
  }
}

class TestStore extends HttpStore {
  constructor(options = {}) {
    super({client: new TestClient(options)})
  }
}

class TestModel extends Model {
  static type() {
    return 'entries'
  }

  static get config() {
    return {
      encrypt: ['message'],
      permit: ['message']
    }
  }

  static get crypto() { return new Cryptos.SimpleCrypto() }

  attributes() {
    return {
      message: this.message
    }
  }
}

describe('HttpStore', function() {
  function store(options = {}) {
    return new TestStore(options)
  }

  describe('#url', function() {
    it('retruns model collection url', function() {
      let url = store().url(TestModel)
      assert.equal(url, '/entries')
    })

    context('with id', function() {
      it('returns model url', function() {
        let url = store().url(TestModel, {id: 1})
        assert.equal(url, '/entries/1')
      })
    })

    it('include directory path', function() {
      const MyModel = class extends TestModel {
              static get directoryModel() {
                return TestModel.fromAttributes({id: 1})
              }
            }
      assert.equal(store().url(MyModel), '/entries/1/entries')
    })

    context('with query', function() {
      it('returns url with query', function() {
        let url = store().url(TestModel, {sort: 'age', limit: 10})
        let urlOrdered = [
          '/entries?sort=age&limit=10',
          '/entries?limit=10&sort=age'
        ]
        assert(urlOrdered.includes(url))
      })

      it('encodes query param', function() {
        let url = store().url(TestModel, {sort: '+age'})
        assert.equal(url, '/entries?sort=%2Bage')
      })

      it('encodes array query param', function() {
        let url = store().url(TestModel, {ids: [1, 2]})
        assert.equal(url, '/entries?ids=1%2C2')
      })
    })

    context('with root', function() {
      it('returns url with root', function() {
        let store = new HttpStore({root: 'http://localhost:3000'})
        let url = store.url(TestModel)
        assert.equal(url, 'http://localhost:3000/entries')
      })
    })
  })

  describe('#findModel', function() {
    it('makes a GET request', async function() {
      const s = store()
      await s.findModel(TestModel, 1)
      let {url, options} = s.cl.calls[0]
      assert.equal(url, s.url(TestModel, {id: 1}))
      assert.deepEqual(options, {
        method: 'GET',
        status: [200, 404]
      })
    })
  })

  describe('#saveModel', function() {
    it('resolves with a model', async function() {
      const model = new TestModel(),
            resolved = await store().saveModel(model)
      assert.equal(resolved, model)
    })

    it('makes a PUT request', async function() {
      const model = new TestModel(),
            s = store()
      model.id = 1
      await s.saveModel(model)
      const {url, options} = s.cl.calls[0]
      assert.equal(options.method, 'PUT')
    })

    context('with new model', async function() {
      it('makes a POST request', async function() {
        const model = new TestModel(),
              s = store()
        model.message = 'hello'
        await s.saveModel(model)
        const {url, options} = s.cl.calls[0]
        assert.deepEqual(options, {
          method: 'POST',
          status: [201, 400],
          body: '{"message":"hello"}'
        })
      })
    })
  })

  describe('#findModels', function() {
    it('makes a GET request', async function() {
      let s = store({resolveWith: []})
      await s.findModels(TestModel)
      let {url, options} = s.cl.calls[0]
      assert.equal(url, s.url(TestModel))
      assert.deepEqual(options, {
        method: 'GET',
        status: [200]
      })
    })

    it('adds query options to url', async function() {
      let s = store({resolveWith: []}),
          q = {limit: 100}
      await s.findModels(TestModel, {query: q})
      let {url} = s.cl.calls[0]
      assert.equal(url, s.url(TestModel) + '?limit=100')
    })

    context('with config findModels method = POST', function() {
      it('makes a POST request', async function() {
        class TestModel extends Model {
          static get config() {
            return {
              findModels: {method: 'POST'}
            }
          }
        }
        const s = store({resolveWith: []})
        await s.findModels(TestModel, {
          query: {localId: '1,2,3'}
        })
        let {url, options} = s.cl.calls[0]
        assert.equal(url, s.url(TestModel) + '/all')
        assert.equal(options.method, 'POST')
        assert.equal(options.body, '{"localId":"1,2,3"}')
      })
    })
  })

  describe('#destroyModel', function() {
    it('makes a DELETE request', async function() {
      const s = store(),
            model = TestModel.fromAttributes({id: 1})
      await s.destroyModel(model)
      const {url, options} = s.cl.calls[0]
      assert.equal(url, '/entries/1')
      assert.deepEqual(options, {method: 'DELETE', status: [200]})
    })
  })

  // TODO: Should be a delegation test. How?
  describe('#signIn', function() {
    it('sets cl headers', function() {
      const s = store()
      s.signIn({login: 'bob@diary', password: 'test'})
      assert(s.cl.headers.Authorization)
    })
  })
})
