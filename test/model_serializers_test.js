require('./test_helper')

let assert = require('assert')

let Model = require('../model')
let Serializers = require('../model_serializers')

class DummyModel extends Model {
  static type() {
    return 'entries'
  }

  attributes() {
    return {
      message: this.message
    }
  }
}

describe('JsonapiSerializer', function() {
  describe('#serialize', function() {
    it('serializes to jsonapi format', function() {
      let serializer = new Serializers.Jsonapi()
      let model = new DummyModel()
      model.id = 1
      model.message = 'hello'
      assert.deepEqual(serializer.serialize(model), {
        data: {
          id: 1,
          type: model.constructor.type(),
          attributes: model.attributes()
        }
      })
    })
  })

  describe('#fromSerialized', function() {
    it('returns model assignable', function() {
      let serializer = new Serializers.Jsonapi()
      let attrs = {
        data: {
          id: 1,
          type: 'entries',
          attributes: {
            message: 'blah'
          }
        }
      }
      let obj = serializer.fromSerialized(attrs)
      assert.deepEqual(obj, {
        id: 1,
        message: 'blah'
      })
    })

    context('with data array', function() {
      it('retuns an array of assignable', function() {
        let serializer = new Serializers.Jsonapi()
        let attrs1 =  {
          type: 'entries',
          id: 1,
          attributes: {
            message: 'hello'
          }
        }
        let dataArray = {
          data: [
           attrs1
          ]
        }

        let array = serializer.fromSerialized(dataArray)
        assert.deepEqual(array, [
          {
            message: 'hello',
            id: 1,
          }
        ])
      })
    })

    context('with includeMeta option', function() {
      it('includes meta', function() {
        let serializer = new Serializers.Jsonapi()
        let attrs = {
          meta: {
            count: 1
          },
          data: {
            id: 1,
            type: 'entries',
          }
        }
        let obj = serializer.fromSerialized(attrs, {includeMeta: true})
        assert.deepEqual(obj, {
          attributes: {id: 1},
          meta: {count: 1}
        })
      })
    })
  })
})
