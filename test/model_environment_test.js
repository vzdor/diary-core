'use strict';

const Environment = require('../model_environment'),
      Model = require('../model')

const assert = require('assert')

describe('ModelEnvironment', function() {
  describe('#model', function() {
    it('returns a new Model', function() {
      const env = new Environment()
      assert(env.model(Model) !== Model)
      assert.equal(env.model(Model).env, env)
      assert(Model.env !== env)
    })

    it('returns the same Model', function() {
      const env = new Environment()
      assert.equal(env.model(Model), env.model(Model))
    })
  })

  describe('#clone', function() {
    it('returns env that makes new Models', function() {
      const env1 = new Environment()
      env1.model(Model)
      const env2 = env1.clone()
      assert(env1.model(Model) !== env2.model(Model))
    })
  })

  describe('#newModel', function() {
    it('returns instance of model', function() {
      const env = new Environment()
      assert(env.newModel(Model) instanceof Model)
    })
  })
})
