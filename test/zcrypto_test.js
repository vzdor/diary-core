'use strict';

const Zcrypto = require('../zcrypto'),
      Core = require('crypto-js/core'),
      HmacSHA256 = require('crypto-js/hmac-sha256'),
      Base64 = require('crypto-js/enc-base64')

const assert = require('assert')

const testKey = '505nT7v2taqhjyWdU5uNaqGi3+n+88R829WyfjB+2qg='

describe('Zcrypto', function() {
  describe('intialize', function() {
    it('resolves with zcrypto', function() {
      const zCrypto = Zcrypto.initialize({key: testKey})
      return zCrypto.then((z) => {
        assert(z instanceof Zcrypto)
      })
    })

    it('derives keys', async function() {
      const z = await Zcrypto.initialize({key: testKey})
      assert.equal(z.keys.length, 1)
      const keys = z.keys.map((k) => k.toString(Base64))
      assert.deepEqual(keys, [
        '78ti740GdMom+Ws95gQMmWd8HyTK+q+U4TLjwNRcIQw='
      ])
    })
  })

  describe('#trapdoor', function() {
    it('resolves with trapdoor', async function() {
      const z = await Zcrypto.initialize({key: testKey}),
            trapdoor = await z.trapdoor('hello')
      assert.deepEqual(trapdoor, [
        'c1kvbHMB4ko/u1JD67SR9Ea2+S3y/aslQL9xPSeV6v0='
      ])
    })
  })

  describe('#codeword', function() {
    it('resolves with codeword', async function() {
      let z = await Zcrypto.initialize({key: testKey}),
          trapdoor = await z.trapdoor('hello'),
          codeword = await z.codeword(trapdoor, '1')
      assert.equal(codeword.length, z.keys.length)
      let x0 = Base64.parse(trapdoor[0]),
          y0 = HmacSHA256('1', x0)
      assert.equal(codeword[0].toString(), y0.toString())
    })
  })
})
