'use strict';

require('./test_helper')

const assert = require('assert')

const {PagedFinder, PagedTextFinder} = require('../paged_finders'),
      Model = require('../model')

class TestModel extends Model {
  static type() { return 'blahs' }
}

describe('PagedFinder', function() {
  describe('#find', function() {
    it('resolves with entries from the store', async function() {
      const e1 = new TestModel(),
            e2 = new TestModel(),
            entries = [e1, e2]
      await e1.save()
      await e2.save()
      const finder = new PagedFinder(TestModel),
            models = await finder.find(0)
      assert.deepEqual(models, entries)
    })
  })
})
