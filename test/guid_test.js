'use strict';

const guid = require('../guid')

const assert = require('assert')

describe('guid', function() {
  it('returns 16 byte string', function() {
    const s = guid()
    assert.equal(s.length, 16)
  })
})
