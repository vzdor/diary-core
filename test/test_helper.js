'use strict';

const Model = require('../model'),
      Diary = require('../diary'),
      TestStore = require('../test_store'),
      Serializers = require('../model_serializers'),
      Cryptos = require('../test_cryptos'),
      Environment = require('../model_environment')

const env = new Environment()

env.store = new TestStore()
env.serializer = new Serializers.Dummy()
env.crypto = new Cryptos.PlainCrypto()
// DiaryRelation models should have a diary and a crypto.
env.diary = Diary.fromAttributes({id: 1})
env.diaryCrypto = new Cryptos.PlainCrypto()
// Default environment.
Model.env = env
