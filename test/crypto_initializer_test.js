'use strict';

const cryptoInitialize = require('../crypto_initializer'),
      JsCrypto = require('../js_crypto')

const assert = require('assert')

describe('cryptoInitialize', function() {
  it('resolves with a JsCrypto', async function() {
    const crypto = await cryptoInitialize({
      // email: 'bob@diary', // key derivation is slow, pass key.
      // password: 'test'
      key: 'KlqsoptTHBK30IhmNSj1mjqKJjzM5BrW24xWV38WT6w='
    })
    assert(crypto instanceof JsCrypto)
  })

  it('sets salt to email:diary', function(done) {
    const Crypto = {}
    Crypto.initialize = function(options) {
      const {salt, password} = options
      assert.equal(salt, 'bob@diary:diary')
      assert.equal(password, '123')
      done()
    }
    cryptoInitialize({email: 'bob@diary', password: '123', Crypto})
  })
})
