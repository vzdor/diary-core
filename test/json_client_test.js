'use strict';

const Client = require('../json_client'),
      Base64 = require('../base64'),
      assert = require('assert')

class TestClient extends Client {
  constructor(response, options = {}) {
    super(options)
    this.response = response
    this.calls = []
  }

  async fetch(url, options) {
    this.calls.push({url, options})
    return this.response
  }
}

function newClient(response = {}, options = {}) {
  const _response = Object.assign({
    status: 200,
    json: () => {}
  }, response)
  return new TestClient(_response, options)
}

describe('JsonClient', function() {
  describe('#request', function() {
    it('calls fetch with url and headers', async function() {
      const cli = newClient({}, {headers: {test: 1}})
      await cli.request('/test/')
      assert.equal(cli.calls[0].url, '/test/')
      assert.deepEqual(cli.calls[0].options, {
        method: 'GET',
        headers: {test: 1},
        body: undefined
      })
    })

    context('with unexpected status', function() {
      it('throws StatusError', function() {
        const cli = newClient({status: 404})
        return cli.request('/test/', {status: [201]}).catch((error) => {
          assert.equal(error.name, 'StatusError')
        })
      })
    })

    context('with 401 status', function() {
      it('throws unauthorized', function() {
        const cli = newClient({status: 401})
        return cli.request('/test/').catch((error) => {
          assert.equal(error.name, 'UnauthorizedError')
        })
      })
    })

    context('with POST and body', function() {
      it('calls fetch with POST and body', async function() {
        const cli = newClient()
        await cli.request('/test/', {
          method: 'POST',
          body: 'payload'
        })
        assert.deepEqual(cli.calls[0].options, {
          method: 'POST',
          headers: {'Content-type': 'application/json'},
          body: 'payload'
        })
      })
    })
  })

  describe('#auth', function() {
    it('sets Authorization header', function() {
      const cli = newClient()
      cli.auth({login: 'bob', password: 'pass'})
      assert.equal(cli.headers.Authorization, 'Basic Ym9iOnBhc3M=')
    })
  })
})
