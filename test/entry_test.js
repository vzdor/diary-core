'use strict';

require('./test_helper')

const Entry = require('../entry'),
      Indexer = require('../zindexer'),
      Cryptos = require('../test_cryptos'),
      Environment = require('../model_environment')

const assert = require('assert')

describe('Entry', function() {
  describe('#attributes', function() {
    it('includes localId and encrypted message', function() {
      let model = new Entry()
      model.localId = 1
      model.encrypted = {message: 'hello'}
      let attrs = model.attributes()
      assert.equal(attrs.localId, 1)
      assert.equal(attrs.message, 'hello')
    })
  })

  describe('#setAttributes', function() {
    it('sets attributes', function() {
      let model = new Entry()
      let obj = {
        id: 1,
        message: 'hello'
      }
      model.setAttributes(obj)
      assert.equal(model.message, 'hello')
    })
  })

  describe('#save', function() {
    after(function() {
      Entry.store.clear()
    })

    context('with new model', function() {
      function newModel() {
        let model = new Entry()
        model.message = 'hello'
        return model
      }

      it('resolves with a saved model', async function() {
        const indexer = new Indexer({env: Entry.env}),
              model = newModel()
        await model.save(indexer)
        assert(!model.isNew())
      })

      it('allocates a localId', async function() {
        const indexer = new Indexer({env: Entry.env}),
              model = newModel()
        await model.save(indexer)
        assert(model.localId)
        assert(Entry.store.pages['localids'][model.localId])
      })

      it('allocates localId using env')

      it('encrypts message using diary crypto', async function() {
        const env = Entry.env.clone()
        env.diaryCrypto = new Cryptos.SimpleCrypto({prefix: 'x-'})
        const model = env.model(Entry).fromAttributes({message: 'hello'}),
              indexer = new Indexer({env: env})
        await model.save(indexer)
        const attrs = Entry.store.pages['entries'][model.id]
        assert(attrs)
        assert.equal(attrs.message, 'x-crypted,hello')
      })

      it('saves with index', async function() {
        let model = newModel(),
            indexer = new Indexer({env: Entry.env})
        await model.save(indexer)
        let attrs = Entry.store.pages['entries'][model.id]
        assert(attrs)
        assert(attrs.index)
        assert.equal(attrs.index, model._index.toJSON())
      })
    })

    it('uses existing localId', async function() {
      const model = new Entry(),
            indexer = new Indexer({env: Entry.env})
      model.localId = 1
      model.message = 'hello'
      await model.save(indexer)
      let localIds = Entry.store.pages['localids']
      assert.equal(localIds, null)
    })
  })
})
