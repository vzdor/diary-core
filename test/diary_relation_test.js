'use strict';

require('./test_helper')

const DiaryRelation = require('../diary_relation'),
      Model = require('../model')

const assert = require('assert')

class TestModel extends DiaryRelation(Model) {}

describe('DiaryRelation', function() {
  it('adds directoryModel', function() {
    assert.equal(TestModel.directoryModel, Model.env.diary)
  })

  it('uses diary crypto', function() {
    assert.notEqual(TestModel.crypto, Model.env.crypto)
    assert.equal(TestModel.crypto, Model.env.diaryCrypto)
  })
})
