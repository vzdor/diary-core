'use strict';

const JsCrypto = require('./js_crypto')

// options.email
// options.password
function initialize(options) {
  const {Crypto = JsCrypto, email, password} = options
  if (email) {
    const _options = Object.assign({}, options, {
      salt: email + ':diary'
    })
    delete _options.email
    return Crypto.initialize(_options)
  }
  return Crypto.initialize(options)
}

module.exports = initialize
