'use strict';

const Model = require('./model'),
      Diary = require('./diary')

class Registration extends Model {
  static type() {
    return 'registrations'
  }

  static get config() {
    return {
      permit: [
        {
          diaries: [ // Assign the ids returned by the store after save.
            ['id']
          ]
        }
      ]
    }
  }

  attributes() {
    return {
      user: {
        email: this.email,
        password: this.constructor.crypto.servicePassword
      },
      diaries: this.diaries.map((diary) => diary.attributes())
    }
  }

  async build() {
    const _Diary = this.constructor.env.model(Diary)
    // Initialize default diaries.
    this.diaries = [
      _Diary.fromAttributes({name: 'Default'}),
      _Diary.fromAttributes({name: 'Secrets', hideListing: true})
    ]
    const ary = this.diaries.map((diary) => diary.build())
    await Promise.all(ary)
  }
}

module.exports = Registration
