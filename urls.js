'use strict';

// options: {key1: 1, key2: 2}
// returns "?key1=1&key2=2"
function urlQuery(options) {
  let keys = Object.keys(options)
      .filter((key) => options[key] !== undefined)
  if (keys.length == 0) return ''
  return '?' + keys.map(
    (key) =>
      key.toString() + '=' + encodeURIComponent(options[key].toString())
  ).join('&')
}

module.exports = {urlQuery}
