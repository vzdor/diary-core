'use strict';

class Entry {
  // wordArray: crypto-js WordArray, a structure with words - an array
  // of 32 bit integers.
  constructor(wordArray) {
    let words = wordArray.words
    this.fp = words[words.length - 1] & 0xffff
    this.int1 = (words[0] >> 16) & 0xffff
    this.int2 = words[0] & 0xffff
    this.int3 = (words[1] >> 16) & 0xffff
  }
}

Entry.max = 0xffff - 1 // Maximum number an entry.int* can take.

module.exports = Entry
