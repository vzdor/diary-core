'use strict';

const Crypto = require('./model_crypto')

const secondaryKey = '505nT7v2taqhjyWdU5uNaqGi3+n+88R829WyfjB+2qg='

class SimpleCrypto extends Crypto {
  constructor({prefix = ''} = {}) {
    super()
    this.prefix = prefix
  }

  encrypt(text) {
    return `${this.prefix}crypted,${text}`
  }

  decrypt(text) {
    let tokens = text.split(',')
    tokens.shift()
    return tokens.join(',')
  }

  hmac(text) {
    return `${this.prefix}hash,${text}`
  }

  get secondaryKey() {
    return secondaryKey
  }

  get servicePassword() {
    return 'my password'
  }
}

class PlainCrypto extends Crypto {
  encrypt(text) {
    return text
  }

  decrypt(text) {
    return text
  }

  hmac(text) {
    return text
  }

  get secondaryKey() {
    return secondaryKey
  }
}

module.exports = {SimpleCrypto, PlainCrypto}
