'use strict';

const stemmer = require('stemtiny/stemmer'),
      Stopwords = require('stemtiny/stopwords'),
      Tokenizer = require('stemtiny/tokenizer')

const Cuckoo = require('./cuckoo'),
      CuckooEntry = require('./cuckoo_entry'),
      Zcrypto = require('./zcrypto'),
      Zmodel = require('./zmodel')

// Goh Z-idx using Cuckoo filter.

class Zindexer {
  // text: text
  // results: unique tokens array
  static tokenize(text) {
    let tokens = Tokenizer.tokenize(text.toLowerCase())
        .filter((token) => ! Stopwords.includes(token))
        .map((token) => stemmer(token)) // to turn off stemmer(word, debug) debugging
    return [... new Set(tokens)]
  }

  // key: Z-index master key, base64 encoded.
  //      crypto.secondaryKey will be used if key is null.
  // env: model environment.
  constructor({env, key} = {}) {
    this.Model = env.model(Zmodel)
    key = key || this.Model.crypto.secondaryKey
    this.zCrypto = Zcrypto.initialize({key})
  }

  // Resolves with {docId1: [token1, token2], docId2: [token2]}.
  async find(text) {
    const tokens = Zindexer.tokenize(text),
          z = await this.zCrypto,
          ary = tokens.map((token) => z.trapdoor(token)),
          trapdoors = await Promise.all(ary),
          models = await this.Model.store.findModels(this.Model, {
            query: {trapdoors: trapdoors}
          })
    return this._format(models, tokens)
  }

  // Resolves with index object.
  addModel(model) {
    return this.addDoc(model.toDoc())
  }

  // Resolves with index object.
  async addDoc(doc) {
    const tokens = Zindexer.tokenize(doc.text),
          z = await this.zCrypto,
          index = new Cuckoo({size: tokens.length, Entry: CuckooEntry}),
          add = async (token) => {
            const trapdoor = await z.trapdoor(token),
                  codeword = await z.codeword(trapdoor, doc.id.toString())
            codeword.forEach((w) => index.add(w))
          },
          ary = tokens.map(add)
    await Promise.all(ary)
    return index
  }

  _format(models, tokens) {
    return models.reduce((results, model) => {
      results[model.id] = []
      for (let i = 0; i < tokens.length; i++) {
        if (model.tokens[i]) results[model.id].push(tokens[i])
      }
      return results
    }, {})
  }
}

module.exports = Zindexer
