'use strict';

class Crypto {
  // Return promises for compatibility with WebCrypto.

  // Encrypts attributes listed in Model.config.encrypt and store
  // them in model.encrypted.

  // Model.config.encrypt can be an array or an object:
  //  {name: 'plain', index: 'json'}
  // 'json' - convert the attribute to string using JSON.stringify.

  // The value should not be undefined if 'json' is used.

  async encryptModel(model) {
    const config = model.constructor.config || {},
          defs = config.encrypt,
          attrs = {},
          encrypted = {}

    if (defs === undefined) return model

    if (defs instanceof Array)
      defs.forEach((el) => attrs[el] = model[el])
    else
      Object.keys(defs).forEach((k) => {
        let value = model[k]
        if (defs[k] === 'json') value = JSON.stringify(value)
        attrs[k] = value
      })

    Object.keys(attrs).forEach((k) => {
      encrypted[k] = this.encrypt(attrs[k])
    })

    model.encrypted = encrypted

    return model
  }

  async decryptModel(attrs, Model) {
    const config = Model.config || {},
          defs = config.encrypt

    if (defs === undefined) return attrs

    const decrypt = (ary) => {
      ary.forEach((el) => {
        if (attrs[el] !== undefined) attrs[el] = this.decrypt(attrs[el])
      })
    }

    if (defs instanceof Array)
      decrypt(defs)
    else {
      const ary = Object.keys(defs)
      decrypt(ary)
      ary.forEach((k) => {
        if (attrs[k] && defs[k] === 'json') attrs[k] = JSON.parse(attrs[k])
      })
    }

    return attrs
  }

  // Convert a password to a key which is more secure to Rainbow attack.
  // pbkdf2 will apply sha256(password) 50000 times, it will take longer to
  // produce a Rainbow table.
  // https://en.wikipedia.org/wiki/Key_stretching
  //
  // password: string
  // key: string
  // Resolves with a base64 string.
  static deriveKey(password, salt) {
    return this.pbkdf2(password, salt, 50000)
  }

  // servicePassword is authentication password used by the Model store.

  // If the servicePassword is discovered by an attacker, it should not
  // be easy to discover the Crypto.key.

  // servicePassword = hmac(passwordKey, '0')

  // Implement

  // constructor({key, passwordKey, macKey, encryptionKey})

  // Resolves with Crypto instance.
  // static initialize({password, salt}) or initialize({key})

  // Base64 stings:

  // property servicePassword

  // property encryptionKey
  // encryptionKey = hmac(key, '0')
  //
  // Refer to Key derivation using PRFs:
  // http://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-108.pdf
  // HMAC is PRF.
  // hmac(key, '1') looks like Counter mode, from the above document,
  // with 1 iteration.

  // property macKey
  // macKey = hmac(key, '1')

  // property passwordKey
  // passwordKey = hmac(key, '2')

  // property secondaryKey
  // secondaryKey is a derived key that can be used in your own
  // security scheme, for example, to derive Zindex keys.

  // Resolves with a new Crypto derived from secondaryKey and user text.
  // async secondaryCrypto({text})

  // If you use secondaryKey, do not use secondaryCrypto() or vice versa.

  // Encrypts using AES and encryptionKey.
  // Returns a bae64 string.
  // text: string.
  // iv: base64 string. If iv is null, a random iv will be made.
  // encrypt(text, {iv} = {})

  // Decrypts using AES.
  // Returns base64 text
  // decrypt(text)

  // Why HMAC
  //
  // An inverted index is:
  // token -> [doc1, doc2, ...]
  // Token is a stem of an English word. I save indexes on the webserver
  // which is not trustworthy.
  // The token should be hidden or the document can be construdcted from
  // the inverted indexes. The array of doc ids is not encrypted.
  //
  // hash(token) -> [doc1, doc2, ...]
  //
  // hash(text) - should be secure against the Rainbow attack.

  // HMAC is secure against known-plaintext attacks.

  // Returns a base64 string. Should use signatureKey.
  // hmac(text)

  // Resolves with a pbkdf2-sha256 base64 string.
  // password: string
  // salt: string
  // iterations: number of iterations.
  // static pbkdf2(password, salt, iterations) { }
}

module.exports = Crypto
