const Store = require('./store')

// Simulates web-server storage.

// #pages[Model.type()][model.id] returns a serialized model.

class TestStore extends Store {
  constructor() {
    super()
    this.pages = {}
  }

  notFound(id) {
    return {
      errors: [
        {
          id: id,
          status: 404
        }
      ]
    }
  }

  clear() {
    this.pages = {}
  }

  async destroyModel(model) {
    // TODO
  }

  async _find(Model, id) {
    const obj = this.__find(id, Model)
    if (obj)
      return Object.assign({}, obj) // Copy
    else
      return this.notFound(id)
  }

  _meta(attrs, options) {
    if (! options.includeMeta) return attrs
    return {
      attributes: attrs,
      meta: options
    }
  }

  // _findAll will not work with Jsonapi serializer.

  // Jsonapi serializer requires data: {data: [{attributes: ...}, ...]}
  // reply.

  async _findAll(klass, options = {}) {
    let col = this.pages[klass.type()]
    if (! col) return this._meta([], options)
    // Collect a copy of each object in the collection
    let array = Object.keys(col).map((id) => Object.assign({}, col[id]))
    return this._meta(array, options)
  }

  __find(id, klass) {
    let collection = this.pages[klass.type()]
    if (collection) return collection[id]
  }

  async _save(model) {
    if (model.isNew())
      return this.add(model)
    if (! this.__find(model.id, model.constructor))
      return this.notFound(model.id)
    let attrs = model.toJSON()
    this.pages[model.constructor.type()][model.id] = attrs
    return Object.assign({}, attrs)
  }

  add(model) {
    let type = model.constructor.type()
    // Find collection
    let col = this.pages[type]
    if (! col) col = this.pages[type] = {}
    // Copy model and assign an id
    let newModel = model.clone()
    newModel.id = Object.keys(col).length + 1
    // Serialize and save the object
    const attrs = newModel.toJSON()
    col[attrs.id] = attrs
    this._addRelations(model, type, attrs)
    // Return a copy
    return Object.assign({}, attrs)
  }

  _addRelations(model, type, attrs) {
    if (type === 'registrations') {
      for (let i = 0; i < model.diaries.length; i++) {
        const diaryAttrs = this.add(model.diaries[i])
        attrs.diaries[i].id = diaryAttrs.id
      }
    }
    return attrs
  }
}

module.exports = TestStore
