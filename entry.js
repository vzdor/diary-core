'use strict';

const Model = require('./model'),
      DiaryRelation = require('./diary_relation')

class LocalIdModel extends DiaryRelation(Model) {
  static type() {
    return 'localids'
  }

  static get config() {
    return {
      permit: ['localId']
    }
  }
}

class Entry extends DiaryRelation(Model) {
  static type() {
    return 'entries'
  }

  static get config() {
    return {
      encrypt: ['message'],
      permit: [
        'localId',
        'message',
        'index',
        'updatedAt', 'createdAt'
      ]
    }
  }

  attributes() {
    const attrs = {
      localId: this.localId,
      message: this.encrypted.message
    }
    if (this._index) attrs.index = this._index.toJSON()
    return attrs
  }

  // Returns an indexable document.
  toDoc() {
    return {
      id: this.localId,
      text: this.message
    }
  }

  async save(indexer) {
    await this._allocateLocalId()
    this._index = await indexer.addModel(this)
    return super.save()
  }

  async _allocateLocalId() {
    if (this.localId) return
    const localId = this.constructor.env.newModel(LocalIdModel)
    await localId.save()
    this.localId = localId.id
  }
}

module.exports = Entry
