require('./app')

const DiaryIndex = require('../async_index'),
      Entry = require('../entry'),
      Index = require('stemtiny/index'),
      {PagedTextFinder, PagedFinder} = require('../paged_finders')

const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const text = process.argv[2]
let finder

if (text && text.length > 0)
  finder = new PagedTextFinder({text})
else
  finder = new PagedFinder(Entry)

function printModels(models) {
  models.forEach((model) => {
    console.log(`#${model.localId} ` + model.message)
    if (model.tokens) console.log(model.tokens.concat([model.score]))
    console.log()
  })
}

function question(page) {
  let q = `Page ${page} of ${finder.pagesCount}. Continue? (y/n) `
  rl.question(q, (answer) => {
    answer == 'y' ? find(page + 1) : rl.close()
  })
}

function find(page = 0) {
  finder.find(page).then((models) => {
    if (models.length) {
      printModels(models)
      question(page)
    } else {
      console.log('Not found.')
      rl.close()
    }
  })
}

find()
