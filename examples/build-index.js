let fs = require('fs')
let path = require('path')

const {initialize} = require('./app')

// corpus is an array: ["text1", "text2", ...]
// ['happy new year', 'hello world']
const corpus = require('../playground/corpus'),
      Indexer = require('../zindexer'),
      Entry = require('../entry')

let env, indexer

let offset = parseInt(process.argv[2] || 0),
      limit = corpus.length - 1
if (process.argv[3]) limit = parseInt(process.argv[3])

function done() {
  console.log('Done.')
}

function validationError(errors) {
  console.log('Validation error.')
  console.log(errors)
}

function load(i) {
  let text = corpus[i]
  let entry = env.newModel(Entry)

  console.log(`Indexing ${i}...`)

  entry.message = text
  entry.save(indexer).then((model) => {
    if (Object.keys(model.errors).length)
      return validationError(model.errors)
    if (i < limit)
      load(i + 1)
    else
      done()
  }).catch((error) => console.log(error))
}

console.log('Bulding index...')
initialize().then((environment) => {
  env = environment
  indexer = new Indexer({env})
  load(offset)
})
