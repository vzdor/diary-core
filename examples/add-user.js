'use strict';

const HttpStore = require('../http_store'),
      Serializers = require('../model_serializers'),
      cryptoInitialize = require('../crypto_initializer'),
      Environment = require('../model_environment'),
      Registration = require('../registration')

// fetch API for nodejs
global.fetch = require('node-fetch')

async function initialize(email, password) {
  const store = new HttpStore({root: 'http://localhost:3000/api'}),
        serializer = new Serializers.Jsonapi(),
        crypto = await cryptoInitialize({email, password})
  const env = new Environment()
  Object.assign(env, {store, serializer, crypto})
  return env
}

const email = process.argv[2],
      password = process.argv[3]

initialize(email, password).then((env) => {
  const registration = env.model(Registration).fromAttributes({email})
  return registration.save().then(() => {
    if (Object.keys(registration.errors).length) {
      console.log(registration.errors)
    } else
      console.log('ok')
  })
}).catch((error) => console.error(error))
