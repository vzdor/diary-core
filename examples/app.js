const HttpStore = require('../http_store'),
      Serializers = require('../model_serializers'),
      JsCrypto = require('../js_crypto'),
      Environment = require('../model_environment'),
      Diary = require('../diary')

// const key = JsCrypto.deriveKey('test', 'bob@diary:diary')
const key = 'KlqsoptTHBK30IhmNSj1mjqKJjzM5BrW24xWV38WT6w='

// fetch API for nodejs
global.fetch = require('node-fetch')

async function initialize() {
  const store = new HttpStore({root: 'http://localhost:3000/api'}),
        serializer = new Serializers.Jsonapi(),
        crypto = await JsCrypto.initialize({key})

  store.signIn({login: 'bob@diary', password: crypto.servicePassword})

  const env = new Environment()
  Object.assign(env, {store, serializer, crypto})

  const diaries = await store.findModels(env.model(Diary)),
        diary  = diaries[0]
  env.diaryCrypto = await diary.deriveCrypto()
  env.diary = diary

  return env
}

module.exports = {initialize}
