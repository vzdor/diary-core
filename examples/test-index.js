'use strict';

// Find N entries
// For each entry, find tokens
// Find indexEntry using entry.message
// deepEqual(tokens, indexedEntry.tokens)

const {initialize} = require('./app')

const Indexer = require('../zindexer'),
      Entry = require('../entry')

const assert = require('assert')

let indexer

function testModel(model, indexTokens) {
  let tokens = indexer.constructor.tokenize(model.message)
  assert.deepEqual(tokens, indexTokens)
  console.log(`${model.localId} ok`)
}

function test(models) {
  if (models.length == 0) return
  let model = models.shift()
  indexer.find(model.message)
    .then((results) => testModel(model, results[model.localId]))
    .then(() => test(models))
}

initialize().then((env) => {
  indexer = new Indexer({env})
  return env.store.findModels(env.model(Entry), {
    query: {page: 0, perPage: 1000}
  })
}).then(test)
