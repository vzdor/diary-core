'use strict';

class Cuckoo {
  constructor({size = 16, Entry} = {}) {
    if (!Entry) throw new Error('Entry should be set.')
    // Entry addressing in the ary is: e.int1 % size.
    // If size > e.int1, then it will not fill the whole ary.
    if (size > Entry.max) throw new Error('size should be less than Entry.max')
    this.size = size
    this.ary = new Array(size)
    this.maxAttempts = 200 // TODO: Why 200? -- 500 used in [1]
    this.Entry = Entry
  }

  add(s) {
    let e = new this.Entry(s)
    return this.addEntry(e)
  }

  addEntry(e) {
    return this._addEntry(e)
  }

  loadFactor() {
    // Count non-null elements.
    let c = this.ary.reduce((count, e) => {
      if (e) count += 1
      return count
    }, 0)
    return c/this.size
  }

  fingerprints() {
    return this.ary.map((e) => e ? e.fp : null)
  }

  toJSON() {
    return this.fingerprints().join(',')
  }

  _addEntry(e, attempt = 0) {
    const i1 = e.int1 % this.size,
          i2 = e.int2 % this.size,
          i3 = e.int3 % this.size

    // If bucket is empty, add entry to the bucket.
    if (this.ary[i1] === undefined) {
      this.ary[i1] = e
      return true
    }
    if (this.ary[i2] === undefined) {
      this.ary[i2] = e
      return true
    }
    if (this.ary[i3] === undefined) {
      this.ary[i3] = e
      return true
    }

    if (attempt > this.maxAttempts) return false

    // Replace the existing entry with new entry and try to add
    // the replaced entry.
    let i = this._random(i1, i2, i3),
        e2 = this.ary[i]
    this.ary[i] = e
    return this._addEntry(e2, attempt + 1)
  }

  _random(i1, i2, i3) {
    const iis = [i1, i2, i3],
          idx = Math.floor(Math.random() * 10) % iis.length
    return iis[idx]
  }
}

class ScalingCuckoo extends Cuckoo {
  constructor(options = {}) {
    super(options)
    this.allEntries = []
  }

  addEntry(e) {
    this.allEntries.push(e)
    if (!this._addEntry(e)) this.scale()
  }

  scale() {
    for (let size = this.size + 16; size < this.Entry.max; size += 16) {
      if (this._scale(size)) return
    }
    throw new Error('Cuckoo scale failed.')
  }

  _scale(newSize) {
    let cuckoo = new Cuckoo({size: newSize, Entry: this.Entry})
    for (let e of this.allEntries) {
      if (! cuckoo._addEntry(e)) return false
    }
    this.size = cuckoo.size
    this.ary = cuckoo.ary
    return true
  }
}

module.exports = ScalingCuckoo

// [1] https://www.cs.cmu.edu/~dga/papers/cuckoo-conext2014.pdf
// [2] https://en.wikipedia.org/wiki/Cuckoo_hashing#Variations
// [3] http://www.lkozma.net/cuckoo_hashing_visualization/
