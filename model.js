'use strict';

const merge = require('lodash.merge')

class Model {
  // Initialize default attributes in the constructor.

  // Returns a string. A db table, or JSONAPI type.
  static type() {
    // implement
  }

  // attrs: can be model.attributes()
  // attrs: can be an array [model1.attributes(), model2.attributes(), ...]
  static fromAttributes(attrs) {
    if (attrs instanceof Array)
      return attrs.map(this.fromAttributes.bind(this))
    else {
      let model = new this()
      model.setAttributes(attrs)
      return model
    }
  }

  // Shortcuts

  static get store() { return this.env.store }

  static get serializer() { return this.env.serializer }

  static get crypto() { return this.env.crypto }

  static find(id) {
    return this.store.findModel(this, id)
  }

  static findModels(options = {}) {
    return this.store.findModels(this, options)
  }

  constructor() {
    // Where to keep encrypted attributes
    this.encrypted = {}
    // Validation or server errors
    this.errors = {}
  }

  isNew() {
    return this.id === undefined
  }

  // Specify which attributes to serialize.
  //
  // Here you can convert an attribute to something:
  // return {
  //   message: 'hello, ' + this.name,
  // }
  // For encrypted attributes:
  // return {
  //   message: this.encrypted.message
  // }
  attributes() {
    return {}
  }

  // Here you can convert something back to what you need.
  setAttributes(attrs) {
    merge(this, attrs)
  }

  toJSON() {
    return this.constructor.serializer.serialize(this)
  }

  // Here you can initialize a model object.
  // The method is called by save() with options.save set to true.
  async build(options = {}) {

  }

  async save() {
    return this.constructor.store.saveModel(this)
  }

  async destroy() {
    return this.constructor.store.destroyModel(this)
  }

  // Returns attributes for cloning.
  state() {
    return this
  }

  // Returns a copy.
  // A copy means that you can set a copy attribute and this will not
  // change the original.

  // Why not newModel.setAttributes(model.attributes())?
  // model.attributes() returns attributes for serialization. It may or
  // may not include all attributes.
  clone() {
    let model = new this.constructor()
    Object.assign(model, this.state())
    return model
  }

  // How to use encryption

  // Assume you want to encrypt subject and message attributes. Define:
  // static get config() { return { {encrypt: ['subject', 'message']} }
  // Call Model.crypto.encryptModel(model)
  // This will encrypt subject and message. You can find encrypted attributes
  // in model.encrypted.

  // Model.store will call crypto.encryptModel on saveModel and will call
  // crypto.decryptModel on findModel and findModels.

  // See comment to the attibutes() method. Note that model[attr] will be used,
  // not model.attributes()[attr].
}

module.exports = Model
