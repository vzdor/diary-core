'use strict';

const attrsFilter = require('diary-attrs-filter')

class Store {
  async findModel(Model, id) {
    const serialized = await this._find(Model, id)
    let attrs = Model.serializer.fromSerialized(serialized)
    attrs = await this._processIncoming(Model, attrs)
    return Model.fromAttributes(attrs)
  }

  // With options.includeMeta, resolves with {models, meta}
  // Resolves with an array of models.
  async findModels(Model, options = {}) {
    const serialized = await this._findAll(Model, options),
          obj = Model.serializer.fromSerialized(serialized, options)

    // Take attributes array and resolve with models.
    const instantiate = (ary) => {
      const model = async (attrs) => {
        attrs = await this._processIncoming(Model, attrs)
        return Model.fromAttributes(attrs)
      }
      return Promise.all(ary.map(model))
    }

    if (options.includeMeta) {
      const {attributes, meta} = obj,
            models = await instantiate(attributes)
      return {meta, models}
    }
    return instantiate(obj)
  }

  async saveModel(model) {
    const Model = model.constructor,
          crypto = Model.crypto
    // Clean errors
    model.errors = {}
    // Initialize
    await model.build({save: true})
    // Encrypt model
    await crypto.encryptModel(model)
    // Save the model in the store, store will return new attributes
    let attrs = await this._save(model)
    // Un-serialize attributes
    attrs = Model.serializer.fromSerialized(attrs)
    attrs = await this._processIncoming(Model, attrs)
    model.setAttributes(attrs)
    return model
  }

  signIn(options) {

  }

  // Process attributes returned by the store.
  async _processIncoming(Model, attrs) {
    const config = Model.config || {}
    attrs = await Model.crypto.decryptModel(attrs, Model)
    if (config.permit)
      attrs = attrsFilter(['id', 'errors'].concat(config.permit), attrs)
    return attrs
  }

  // Implement

  // destroyModel
  // _find
  // _findAll
  // _save
}

module.exports = Store
